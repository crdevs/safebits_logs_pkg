<?php

namespace Safebits\Logs\Exceptions;

/**
 * Class NoKeyFoundException
 * @package Safebits\Logs\Exceptions
 */
class InvalidKeyException extends \Exception
{
    /**
     * InvalidDataTypeException constructor.
     * @param $message
     */
    public function __construct($message = null)
    {
        $message = $message ? $message : 'Invalid system key provide';
        parent::__construct(500, $message);
    }
}
