<?php

namespace Safebits\Logs\Exceptions;

/**
 * Class NoKeyFoundException
 * @package Safebits\Logs\Exceptions
 */
class InvalidCryptoDivisionException extends \Exception
{
    /**
     * InvalidDataTypeException constructor.
     * @param $message
     */
    public function __construct($message = null)
    {
        $message = $message ? $message : 'Invalid system crypto division provide';
        parent::__construct(500, $message);
    }
}
