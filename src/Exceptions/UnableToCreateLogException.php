<?php

namespace Safebits\Logs\Exceptions;

/**
 * Class NoKeyFoundException
 * @package Safebits\Logs\Exceptions
 */
class UnableToCreateLogException extends \Exception
{
    /**
     * InvalidDataTypeException constructor.
     * @param $message
     */
    public function __construct($message = null)
    {
        $message = $message ? $message : 'Unable to create api request log';
        parent::__construct(500, $message);
    }
}
