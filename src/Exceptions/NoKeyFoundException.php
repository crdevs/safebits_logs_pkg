<?php

namespace Safebits\Logs\Exceptions;

/**
 * Class NoKeyFoundException
 * @package Safebits\Logs\Exceptions
 */
class NoKeyFoundException extends \Exception
{
    /**
     * InvalidDataTypeException constructor.
     * @param $message
     */
    public function __construct($message = null)
    {
        $message = $message ? $message : 'No system key provide';
        parent::__construct(500, $message);
    }
}
