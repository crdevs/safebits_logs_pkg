<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections you wish
    | to use as your default connection for all common migrations, seeders,
    | models and so on.
    |
    */
    'connection' => env('DB_CONNECTION', 'mysql'),


    /*
    |--------------------------------------------------------------------------
    | Credential nomenclature
    |--------------------------------------------------------------------------
    |
    | With this configurations, you'll be able to let the package know the headers,
    | table/model name, and credential entity id (clientDivisionId , systemId, ...)
    | for consider the authentication process for different systems
    |
    */
    'key_header' => '',
    'access_header' => '',
    'nonce_header' => '',
    'signature_header' => '',
    'entity_credential_url' => '',
    'entity_id_name' => '',

];
