<?php

namespace Safebits\Logs\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Safebits\Common\Constants\CommonConstants;
use Safebits\Common\Helpers\AuthHelper;
use Safebits\Common\Models\ClientDivision;
use Safebits\Common\Models\ClientDivisionCredential;
use Safebits\Logs\Exceptions\InvalidKeyException;
use Safebits\Logs\Exceptions\UnableToCreateLogException;
use Safebits\Logs\Models\ApiLogs;
use Safebits\Logs\Exceptions\NoKeyFoundException;
use Safebits\Logs\Exceptions\InvalidCryptoDivisionException;
use Carbon\Carbon;

/**
 * Class LogAPIRequest
 * @package App\Http\Middleware
 */
class LogAPIRequest
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws UnableToCreateLogException
     */
    public function handle(Request $request, Closure $next, $onlyWhenFullLog = false)
    {
        $request->headers->set('startTime', Carbon::now(), false);
        if(!$onlyWhenFullLog || ($onlyWhenFullLog && env('SB_FULL_API_LOGS', false))){
            $this->createLog($request);
        }

        return $next($request);
    }

    /**
     * @param $request
     * @return void
     * @throws UnableToCreateLogException
     */
    private function createLog($request){
        try {
            $method = Arr::last(explode('\\', $request->route()->getActionName()));
            $method = $request->getMethod() . ': ' . $method;
            $log = new ApiLogs();

            if (env('APP_ENV') == 'production') {
                $ipAddress = $request->header('X-Forwarded-For');
            } else {
                $ipAddress = $request->ip();
            }

            $serializedRequest = $this->serializeRequest($request);
            $log->ipAddress = $ipAddress;
            $log->method = $method;
            $log->url = $request->url();
            $log->entityId = $this->getEntityId($request);
            $log->request = $serializedRequest;
            $log->requestHeaders = $this->serializeRequestHeader($request);
            $log->save();


            $request->headers->set('logId', $log->apiLogId);
        } catch (\Exception $ex) {
            throw new UnableToCreateLogException($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function terminate($request, $response)
    {
        try {
            $logId = $request->headers->get('logId');
            $startTime = new Carbon($request->headers->get('startTime'));

            if ($logId) {
                $log = ApiLogs::find($logId);

                if ($log) {
                    $time = $startTime->diffInSeconds(Carbon::now());
                    $responseContent = $response->getContent();
                    $log->response = $responseContent; // TODO: scratch
                    $log->httpStatus = $response->getStatusCode();
                    $log->responseTime = $time;
                    $log->save();
                }
            }
        } catch (\Exception $ex) {
        }
    }

    /**
     * @param Request $request
     * @return int|mixed
     * @throws InvalidCryptoDivisionException
     * @throws InvalidKeyException
     * @throws NoKeyFoundException
     */
    protected function getEntityId(Request $request)
    {
        if($request->hasHeader(CommonConstants::X_CRYPTO_DIVISION)){
            return $this->getClientDivisionIdByHeader($request);
        }else{
            return $this->getEntityIdByKey($request);
        }
    }

    /**
     * @param $request
     * @return mixed
     * @throws InvalidCryptoDivisionException
     */
    public function getClientDivisionIdByHeader(Request $request){
        $cryptoDivisionId = $request->header(CommonConstants::X_CRYPTO_DIVISION);
        $clientDivision = ClientDivision::find($cryptoDivisionId);

        if($clientDivision === null){
            throw new InvalidCryptoDivisionException();
        }

        return $cryptoDivisionId;
    }

    /**
     * @return int|mixed
     * @throws InvalidKeyException
     * @throws NoKeyFoundException
     */
    public function getEntityIdByKey(Request $request){
        $key = AuthHelper::getKeyHeader($request);
        $modelUrl = config('safebits_logs.entity_credential_url');

        if (!$key) {
            throw new NoKeyFoundException();
        }
        $entityCredentials = $modelUrl::where(['key' => $key, 'isActive' => 1])->first();
        if ($entityCredentials === null) {
            throw new InvalidKeyException();
        }

        return $entityCredentials[config('safebits_logs.entity_id_name')];
    }

    /**
     * @param Request $request
     * @return false|string
     */
    protected function serializeRequest(Request $request)
    {
        return json_encode(['content' => $request->getContent(), 'params' => $request->all()]);
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function serializeRequestHeader(Request $request)
    {
        $access = $request->header(config('safebits_logs.access_header'));
        $key = AuthHelper::getKeyHeader($request);
        $nonce = AuthHelper::getNonceHeader($request);
        $signature = AuthHelper::getSignatureHeader($request);

        return json_encode([
            'key' => $key,
            'nonce' => $nonce,
            'signature' => $signature,
            'access' => $access,
        ]);
    }
}
