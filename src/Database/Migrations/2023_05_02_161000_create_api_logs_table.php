<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;


/**
 * Class CreateSystemTable
 */
class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('safebits_logs.connection'))->create('api_logs', function (Blueprint $table) {
            $table->increments('apiLogId');
            $table->string('ipAddress')->nullable();
            $table->string('method');
            $table->string("url");
            $table->integer('clientDivisionId');
            $table->binary("request")->nullable();
            $table->binary('requestHeaders')->nullable();
            $table->binary("response")->nullable();
            $table->integer("httpStatus")->nullable();
            $table->integer("responseTime")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('api_logs');
    }
}
