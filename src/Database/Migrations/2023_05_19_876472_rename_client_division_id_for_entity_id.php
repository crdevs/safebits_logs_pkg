<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;


/**
 * Class CreateSystemTable
 */
class RenameClientDivisionIdForEntityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::connection(config('safebits_logs.connection'))->table('api_logs', function (Blueprint $table) {
            $table->renameColumn('clientDivisionId', 'entityId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('safebits_logs.connection'))->table('api_logs', function (Blueprint $table) {
            $table->renameColumn('entityId', 'clientDivisionId');
        });
    }
}
