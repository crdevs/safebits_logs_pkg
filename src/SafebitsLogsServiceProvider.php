<?php

namespace Safebits\Logs;

use Illuminate\Support\ServiceProvider;
use Safebits\Logs\Commands\MigrateLogs;

/**
 * Class SafebitsLogsServiceProvider
 * @package Safebits\Logs
 */
class SafebitsLogsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // TODO implement
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Registers provider commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateLogs::class,
            ]);
        }

        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_logs.php' => config_path('safebits_logs.php'),
        ]);
    }
}
