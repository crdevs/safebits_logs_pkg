<?php

namespace Safebits\Logs\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $apiLogId
 * @property string|null $ipAddress
 * @property string $method
 * @property string|null $url
 * @property int|null $entityId
 * @property mixed $request
 * @property mixed|null $requestHeaders
 * @property mixed|null $response
 * @property int|null $httpStatus
 * response time is messure in seconds
 * @property int|null $responseTime
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereApiLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereClientDivisionlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereRequestHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereHttpStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereResponseTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Logs\Models whereUrl($value)
 * @mixin \Eloquent
 */
class ApiLogs extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'apiLogId';

    /**
     * ApiLog constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection(config('safebits_logs.connection'));
        $this->setTable('api_logs');
    }
}
